import Vue from 'vue'
import Router from 'vue-router'

import store from '@/store/index'

import PlaylistsList from '@/modules/music/index'
import Playlist from '@/components/Playlist'
import PlaylistCreate from '@/components/Playlist/Create'
import Genres from '@/components/Genres'
import UsersList from '@/components/UsersList'
import Home from '@/modules/home/index'
import NotFound from '@/modules/NotFound/index'

Vue.use(Router)

export const constantRouterMap = [
  {
    path: '/',
    component: Home,
    name: 'home',
    meta: {
      title: 'Главная',
      menuKey: 'home'
    }
  },
  {
    path: '/my-music',
    name: 'my-music',
    component: PlaylistsList,
    meta: {
      title: 'Моя музыка',
      menuKey: 'playlists'
    },
    children: [
      {
        path: '/my-music/create',
        component: PlaylistCreate
      },
      {
        path: '/my-music/:playlistId',
        component: Playlist,
        meta: {
          title: 'Playlist'
        }  
      }
    ]
  },
  {
    path: '/genres',
    component: Genres,
    name: 'genres',
    meta: {
      title: 'Создать жанр',
      menuKey: 'genres'
    },
  },
  {
    path: '/users-list',
    component: UsersList,
  },
  { 
    path: '/404', 
    name: '404', 
    component: NotFound, 
  },
  { 
    path: '*', 
    redirect: '/404' 
  }
]

const router = new Router({
  routes: constantRouterMap
})

router.beforeEach((to, from, next) => {
  const isMusic = to.matched[0] && to.matched[0].path
  console.log(to.path)
  if (to.path === '/my-music/create' && !store.state.user.isLogged){
    next({path: from.path})
    return
  }

  if (to.path !== '/' && isMusic !== '/my-music' && !store.state.user.isLogged){
    next({path: from.path})
    return
  }

  if(to.path === '/genres' && !store.state.user.roles.includes('ROLE_ADMIN')){
    next({path: from.path})
    return
  }
  
  if(to.path === '/users-list' && !store.state.user.roles.includes('ROLE_ADMIN')){
    next({path: from.path})
    return
  }

  next()
})

export default router
