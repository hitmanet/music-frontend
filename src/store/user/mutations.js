export default {
  SET_TOKEN: (state, { token }) => {
    state.token = token
  },
  SET_LOGIN: (state, login) => {
    state.isLogged = login
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
    state.isAdmin = roles.includes('ROLE_ADMIN')
    state.isModer = roles.includes('ROLE_MODER')
  },
  SET_USER_INFO: (state, userInfo) => {
    state.userInfo = userInfo
  },
  SET_LOADING: (state, loading) => {
    state.loading = loading
  },
  UPDATE_USER_INFO: (state, userInfo) => {
    state.userInfo = { ...state.userInfo, ...userInfo }
  }
}
