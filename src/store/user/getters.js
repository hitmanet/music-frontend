export default {
  isLogged: state => state.isLogged,
  userInfo: state => state.userInfo,
  loading: state => state.loading,
  token: state => state.token,
  isAdmin: state => state.isAdmin,
  isModer: state => state.isModer,
  roles: state => state.roles
}
