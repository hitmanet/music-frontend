import { login, getUserInfo, register } from '@/api/auth'
import { setToken, removeToken } from '@/utils/auth'

export default {
  async loginByUsername ({ commit }, userInfo){
    const { email, password } = userInfo
    const response = await login(email.trim(), password)
    commit('SET_TOKEN', response.data)
    setToken(response.data)
  },

  async setLogin ({ commit }){
    commit('SET_LOGIN', true)
  },

  async signUp ({ commit }, userInfo){
    const response = await register(userInfo)
    setToken(response.data)
    commit('SET_TOKEN', response.data)
  },

  async getCurrent ({ commit, dispatch }){
    try {
      const response = await getUserInfo()
      const { data, data: { email, name, created_at, id } } = response
      commit('SET_ROLES', data.roles)
      commit('SET_LOGIN', true)
      commit('SET_USER_INFO', {
        created_at,
        email,
        name,
        id
      })
      return response
    } catch (error){
      dispatch('userLogout')
    }
  },

  userLogout ({ commit }){
    commit('SET_TOKEN', '')
    commit('SET_ROLES', [])
    commit('SET_USER_INFO', null)
    commit('SET_LOGIN', false)
    removeToken()
  },

  setTokenToStore({ commit }, token){
    commit('SET_TOKEN', token)
  }
}
