export default () => ({
  isAdmin: false,
  isModer: false,
  isLogged: false,
  userInfo: null,
  token: null,
  loading: false,
  roles: []
})
