import { getCommentsByPlaylistId } from '@/api/comments'

export default {
  async getComments({ commit }, { id, page, limit }){
     const response = await getCommentsByPlaylistId({
      id,
      page: page,
      limit: limit,
    })

    commit('SET_COMMENTS', response.data)
  }
}