import Cookies from 'js-cookie'

export default {
  TOGGLE_SIDEBAR: state => {
    Cookies.set('sidebarStatus', +!state.sidebar.opened)
    state.sidebar.opened = !state.sidebar.opened
  },
  SET_SIDEBAR_ITEMS: (state, newItems) => {
    state.sidebar.items = newItems
  },
  SET_LANGUAGE: (state, language) => {
    state.language = language
    Cookies.set('language', language)
  },
  ADD_VISITED_VIEWS: (state, view) => {
    if (state.visitedViews.some(v => v.path === view.path)) return
    state.visitedViews.push({
      name: view.name,
      path: view.path,
      title: view.meta.title || 'no-name'
    })
    if (!view.meta.noCache){
      state.cachedViews.push(view.name)
    }
  },
  DEL_VISITED_VIEWS: (state, view) => {
    for (const [i, v] of state.visitedViews.entries()){
      if (v.path === view.path){
        state.visitedViews.splice(i, 1)
        break
      }
    }
    for (const i of state.cachedViews){
      if (i === view.name){
        const index = state.cachedViews.indexOf(i)
        state.cachedViews.splice(index, 1)
        break
      }
    }
  }
}
