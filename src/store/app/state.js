import Cookies from 'js-cookie'

export default {
  sidebar: {
    opened: !+Cookies.get('sidebarStatus'),
    newItems: null
  },
  constantRoutes: [],
  language: Cookies.get('language') || 'ru',
  visitedViews: [],
  cachedViews: []
}
