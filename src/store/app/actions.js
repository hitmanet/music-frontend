export default {
  toggleSideBar ({ commit }){
    commit('TOGGLE_SIDEBAR')
  },
  setLanguage ({ commit }, language){
    commit('SET_LANGUAGE', language)
  },
  addVisitedViews ({ commit }, view){
    commit('ADD_VISITED_VIEWS', view)
  },
  delVisitedViews ({ commit, state }, view){
    return new Promise((resolve) => {
      commit('DEL_VISITED_VIEWS', view)
      resolve([...state.visitedViews])
    })
  }
}
