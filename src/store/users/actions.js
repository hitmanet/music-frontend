import { getUsers } from '@/api/users'

export default {
  async getUsersList({ commit }, { page, limit, orderType, orderColumn }){
    const response = await getUsers({ page, limit, orderType, orderColumn })
    commit('SET_USERS', response.data)
  }
}