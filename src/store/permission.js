// import { constantRouterMap, asyncRouterMap } from '@/router'

// function hasPermission (userRoles, route){
//   if (route.meta && route.meta.roles){
//     if (route.meta.onlyAuthenticated){
//       return true
//     }
//     if (route.meta.roles){
//       let includes = false
//       userRoles.forEach(userRole => {
//         route.meta.roles.forEach(routeRole => {
//           if (routeRole === userRole){
//             includes = true
//           }
//         })
//       })
//       return includes
//     }
//     return true
//   }
//   return true
// }

// const filterAsyncRouter = (asyncRouterMap, roles) => {
//   return asyncRouterMap.filter(route => {
//     if (hasPermission(roles, route)){
//       if (route.children && route.children.length){
//         route.children = filterAsyncRouter(route.children, roles)
//       }
//       return true
//     }
//     return false
//   })
// }

// const permission = {
//   state: {
//     routers: constantRouterMap,
//     addRouters: []
//   },
//   mutations: {
//     SET_ROUTERS: (state, routers) => {
//       state.addRouters = routers
//       state.routers = constantRouterMap.concat(routers)
//     }
//   },
//   actions: {
//     generateRoutes ({ commit }, data){
//       return new Promise(resolve => {
//         const { roles } = data
//         let accessedRouters
//         if (roles.indexOf('ROLE_ADMIN') >= 0 || roles.indexOf('ROLE_SUPPORT') > 0){
//           accessedRouters = asyncRouterMap
//         } else {
//           accessedRouters = filterAsyncRouter(asyncRouterMap, roles)
//         }
//         commit('SET_ROUTERS', accessedRouters)
//         resolve()
//       })
//     },
//     ResetRoutes ({ commit }){
//       commit('SET_ROUTERS', [])
//     }
//   }
// }

// export default permission
