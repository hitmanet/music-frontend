import { getFavoritesPlaylist, getPlaylistsByUserId } from '@/api/playlists'
export default {
  async getFavPlaylists({ commit }, { userId, page, limit }){
    const response = await getFavoritesPlaylist(userId, page, limit)
    commit('SET_PLAYLISTS', {
      data: response.data.data,
      where: 'playlistsFav'
    })
  },
  async getPlaylists({ commit }, { user }){
    const response = await getPlaylistsByUserId(user)
    commit('SET_PLAYLISTS', {
      data: response.data.data,
      where: 'playlists'
    })
  }
}