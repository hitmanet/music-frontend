import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'
import app from './app'
import playlist from './playlist'
import users from './users'
import comments from './comments'
import getVersion from '@/utils/getVersion'

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    app,
    user,
    playlist,
    users,
    comments
  },
  getters: {
    //permission_routers: state => state.permission.routers,
    version: () => getVersion()
  }
})
