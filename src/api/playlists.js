import request from '@/utils/request'

export const getPlaylists = () => (
  request({
    url: '/api/playlists',
    method: 'get'
  })
)

export const deletePlaylist = id => (
  request({
    url: `/api/playlists/${id}`,
    method: 'delete'
  })
)

export const getPlaylistsById = (id, userId = '') => (
  request({
    url: `/api/playlists/${id}`,
    method: 'get',
    params: {
      userId
    }
  })
)

export const getPlaylistsByUserId = id => (
  request({
    url: '/api/playlists',
    method: 'get',
    params: {
      user: id
    }
  })
)

export const postPlaylistToFavorites = id => (
  request({
    url: '/api/favorites',
    method: 'post',
    params: {
      playlistId: id
    }
  })
)

export const deletePlaylistFromFavorites = id => (
  request({
    url: '/api/favorites',
    method: 'delete',
    params: {
      playlistId: id
    }
  })
)

export const getFavoritesPlaylist = (userId, page, limit) => (
  request({
    url: '/api/favorites',
    method: 'get',
    params: {
      userId,
      page,
      limit
    }
  })
)

export const getPlaylistsByQuery = (orderType, orderColumn) => (
  request({
    url: '/api/playlists',
    method: 'get',
    params: {
      orderType,
      orderColumn
    }
  })
)

export const putPlaylistRating = (id, value) => (
  request({
    url: `/api/playlists/${id}`,
    method: 'put',
    params: {
      rating: value
    }
  })
)
