import request from '@/utils/request'

export const getNameFile = (data, type) => {
  return request({
    url: `/api/${type}`,
    method: 'post',
    data
  })
}

export const uploadFile = (data, to) => {
  return request({
    url: `/api/${to}`,
    method: 'post',
    data
  })
}

export const getPlaylists = () => {
  return request({
    url: '/api/playlists',
    method: 'get'
  })
}

export const getGenres = (limit, page) => {
  return request({
    url: '/api/genres',
    method: 'get',
    params: {
      limit,
      page
    }
  })
}

export const getGenreById = id => {
  return request({
    url: `/api/genres/${id}`,
    method: 'get'
  })
}

export const addGenre = data => {
  return request({
    url: '/api/genres',
    method: 'post',
    data
  })
}

export const deleteGenre = id => {
  return request({
    url: `/api/genres/${id}`,
    method: 'delete'
  })
}