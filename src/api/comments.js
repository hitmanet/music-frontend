import request from '@/utils/request'

export const getCommentsByPlaylistId = ({ id, page, limit }) =>{
  return request({
    url: `/api/comments/`,
    method: 'get',
    params: {
      playlistId: id,
      page,
      limit
    }
  })
}
