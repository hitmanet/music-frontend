import request from '@/utils/request'

export const login = (email, password) => {
  return request({
    url: '/api/login',
    method: 'get',
    params: {
      email,
      password
    }
  })
}

export const register = data =>{
  return request({
    url: '/api/register',
    method: 'post',
    data
  })
}

export const logout = () => {
  return request({
    url: 'login/logout',
    method: 'post'
  })
}

export const getUserInfo = () =>{
  return request({
    url: '/api/current',
    method: 'get'
  })
}

export const getUserById = id =>{
  return request({
    url: `/api/users/${id}`,
    method: 'get'
  })
}
