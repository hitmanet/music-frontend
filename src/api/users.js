import request from '@/utils/request'

export const getUsers = ({ page, limit, orderColumn = '', orderType = '' }) => {
  return request({
    url: `/api/users`,
    method: 'get',
    params: {
      limit,
      page,
      orderColumn,
      orderType
    }
  })
}

export const deleteUser = ({ userId }) => {
  return request({
    url: `/api/users/${userId}`,
    method: 'delete',
  })
}

export const changeRoles = ({ userId, roles }) => {
  return request({
    url: `/api/users/${userId}`,
    method: 'put',
    params: {
      roles
    }
  })
}