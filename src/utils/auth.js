const REFRESH_TOKEN = 'refresh-token'

export const getToken = () => localStorage.getItem(REFRESH_TOKEN)

export const setToken = ({ token }) => {
  localStorage.setItem(REFRESH_TOKEN, token)
}

export const removeToken = () => {
  localStorage.removeItem(REFRESH_TOKEN)
}

export const getRefreshToken = () => localStorage.getItem(REFRESH_TOKEN)
