import axios from 'axios'
import {Message} from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'
import baseHeaders from '@/utils/baseHeaders'

const service = axios.create({
  baseURL: 'http://symfony.dev.webant.ru'
})

const serviceDecorator = config => new Promise(async (resolve, reject) => {
  const configSettings = { ...config }
  try {
    console.log(configSettings)
    const response = await service(configSettings)
    resolve(response)
  } catch (e){
    reject(e)
  }
})


service.interceptors.request.use(config => {
  config.headers = { ...baseHeaders }
  const token = store.getters['user/token'] || getToken()
  if (token){
    config.headers['Authorization'] = `Bearer ${token}`
  }
  return config
}, error => {
  console.log('error on request', error)
  return Promise.reject(error)
})

service.interceptors.response.use(
  response => {
    return response
  },
  async (error) => {
    if (error.response && error.response.status === 401){

      if(error.response && error.response.data && error.response.data.error.password){
        Message.error('Неправильно введён пароль.')
      } else if(error.response && error.response.data && error.response.data.error.email){
        Message.error('Пользователь с такой почтой уже существует.')
      } else {
        Message.error('Пожалуйста, пройдите авторизацию.')
      }
    }

    if (error.response && error.response.status === 403){
      Message.error('Доступ запрещён')
      return error
    }

    if (error.response && error.response.status === 500){
      Message.error('Упс, с сервером что-то не так, повторите попытку позже')
      return error
    }

    return new Promise((_, reject) => {
      reject(error)
    })
  
  })

export default serviceDecorator
