import Vue from 'vue'
import VueSocketIO from 'vue-socket.io'
import ElementUI from 'element-ui'

import App from './App'
import router from './router'
import store from './store'

import vmodal from 'vue-js-modal'

import 'element-ui/lib/theme-chalk/index.css'
import 'vue-good-table/dist/vue-good-table.css'

import { getToken } from '@/utils/auth'

Vue.use(ElementUI)
Vue.use(vmodal)
Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://109.196.164.214:3000',
  vuex: {
      store,
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_'
  },
  options: {
    query: {
      auth: `Bearer ${getToken()}`
    }
  }
}))

Vue.config.productionTip = false // permissions

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
