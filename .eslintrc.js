module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/essential",
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
      'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'no-empty': 'error',
      'quotes': ['error', 'single', { allowTemplateLiterals: true }],
      'eqeqeq': ['error', 'always'],
      'no-else-return': ['error', { allowElseIf: false }],
      'no-empty-function': 'error',
      'no-unused-vars': 'error',
      'semi': ['error', 'never'],
      'no-var': 'error',
      'space-before-blocks': ["error", "never"],
    },
    parserOptions: {
      parser: 'babel-eslint'
    }  
}