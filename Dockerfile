FROM node:10.15.0
 
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
 
ENV PATH /usr/src/app/node_modules/.bin:$PATH
 
COPY package.json /usr/src/app/package.json
COPY yarn.lock /usr/src/app/yarn.lock


ENV VUE_APP_AUTH_PATH  /jwt
ENV VUE_APP_API_PATH /
ENV VUE_APP_UPLOADS_PATH  /uploads
ENV VUE_APP_APP_HOST 0.0.0.0
ENV VUE_APP_APP_PORT 3000
ENV VUE_APP_API_URL https://tizer.dev.webant.ru

COPY . /usr/src/app/

RUN yarn
RUN yarn add @vue/cli

CMD ["yarn", "serve"]