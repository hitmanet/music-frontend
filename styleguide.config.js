module.exports = {
  sections: [
    {
      name: 'Views',
      components: 'src/views/**/[A-Z]*.vue'
    },
    {
      name: 'Ui Components',
      components: 'src/components/**/[A-Z]*.vue'
    }
  ]
}
